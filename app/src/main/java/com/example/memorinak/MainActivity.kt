package com.example.memorinak


import android.app.ActionBar
import android.app.AlertDialog
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.marginRight
import androidx.core.view.updateLayoutParams
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Math.random

class MemoryGameCard(val name:String, val layout: ConstraintLayout, val image: ImageView, val text: TextView, val back: ImageView) {
    var removed = false
    var shown = false
    fun flip() {
        shown = !shown

        image.visibility = when (shown) {
            true -> View.VISIBLE
            false -> View.INVISIBLE
        }

        text.visibility = when (shown) {
            true -> View.VISIBLE
            false -> View.INVISIBLE
        }

        back.visibility = when (shown) {
            true -> View.INVISIBLE
            false -> View.VISIBLE
        }
    }

    fun remove() {
        layout.visibility = View.INVISIBLE
        image.visibility = View.INVISIBLE
        back.visibility = View.INVISIBLE

        shown = false
        removed = true
    }
}

class MainActivity : AppCompatActivity() {
    val cards = ArrayList<MemoryGameCard>()
    var flippedNow:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        val layout = LinearLayout(applicationContext)
        layout.orientation = LinearLayout.VERTICAL

        val params = LinearLayout.LayoutParams(0, 400)
        params.weight = 1.toFloat() // единичный вес
        params.gravity = Gravity.CENTER

        val pictureMap = mapOf(
            "alpha" to R.drawable.alpha,
            "beta" to R.drawable.beta,
            "gamma" to R.drawable.gamma,
            "delta" to R.drawable.delta,
            "epsilon" to R.drawable.epsilon,
            "zeta" to R.drawable.zeta,
            "eta" to R.drawable.eta,
            "theta" to R.drawable.theta,
            "iota" to R.drawable.iota,
            "kappa" to R.drawable.kappa,
            "lambda" to R.drawable.lambda,
            "mu" to R.drawable.mu,
            "nu" to R.drawable.nu,
            "xi" to R.drawable.xi,
            "omicron" to R.drawable.omicron,
            "pi" to R.drawable.pi,
            "rho" to R.drawable.rho,
            "sigma" to R.drawable.sigma,
            "tau" to R.drawable.tau,
            "upsilon" to R.drawable.upsilon,
            "phi" to R.drawable.phi,
            "chi" to R.drawable.chi,
            "psi" to R.drawable.psi,
            "omega" to R.drawable.omega
        )

        val pictures = pictureMap.keys.toTypedArray()
        var picturesAvail = pictures.size

        fun getRandomImage():String {
            val idx = (picturesAvail * random()).toInt()

            picturesAvail--

            val t = pictures[idx]
            pictures[idx] = pictures[picturesAvail]

            return t
        }

        fun createCard(name:String, imageId:Int):MemoryGameCard {
            val cardLayout = ConstraintLayout(applicationContext).apply {
                layoutParams = params.apply {
                    setMargins(32, 32, 32, 32)
                }
            }

            val cardImage = ImageView(applicationContext).apply {
                setImageResource(imageId)
                layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                visibility = View.INVISIBLE

                id = View.generateViewId()
            }

            val cardText = TextView(applicationContext).apply {
                layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
                text = name
                visibility = View.INVISIBLE

                id = View.generateViewId()
            }

            val cardBack = ImageView(applicationContext).apply {
                setImageResource(R.drawable.pattern)
                layoutParams = ConstraintLayout.LayoutParams(0, 0)
            }

            cardLayout.apply {
                setBackgroundColor(Color.WHITE)
                addView(cardImage)
                addView(cardText)
                addView(cardBack)
                setOnClickListener(colorListener)

                // Took what, two hours to figure this out?
                id = View.generateViewId()
            }

            cardImage.updateLayoutParams<ConstraintLayout.LayoutParams> {
                endToEnd = cardLayout.id
                startToStart = cardLayout.id
                topToTop = cardLayout.id
                bottomToBottom = cardLayout.id
            }

            cardText.updateLayoutParams<ConstraintLayout.LayoutParams> {
                endToEnd = cardLayout.id
                startToStart = cardLayout.id
                topToBottom = cardImage.id

                topMargin = 24
            }

            cardBack.updateLayoutParams<ConstraintLayout.LayoutParams> {
                endToEnd = cardLayout.id
                startToStart = cardLayout.id
                topToTop = cardLayout.id
                bottomToBottom = cardLayout.id
            }

            val card = MemoryGameCard(name, cardLayout, cardImage, cardText, cardBack)

            card.layout.tag = card

            return card
        }

        for (i in 1..8) {
            val name = getRandomImage()
            val imageId = pictureMap[name] ?: -1

            cards.add(createCard(name, imageId))
            cards.add(createCard(name, imageId))
        }

        cards.shuffle()

        val rows = Array(4, { LinearLayout(applicationContext)})

        var count = 0
        for (card in cards) {
            val row: Int = count / 4
            rows[row].addView(card.layout)
            count ++
        }
        for (row in rows) {
            layout.addView(row)
        }
        setContentView(layout)
    }

    suspend fun compareFlipped() {
        val visibleCards = cards.filter { card -> card.shown }
        val card_a = visibleCards[0]
        val card_b = visibleCards[1]

        if (card_a.name == card_b.name) {
            delay(1000)
            card_a.remove()
            card_b.remove()
            flippedNow = 0
        } else {
            delay(1000)
            card_a.flip()
            card_b.flip()
            flippedNow = 0
        }
    }

    fun winConditionCheck() {
        val remainingCards = cards.filter { card -> !card.removed }

        if (remainingCards.size == 0) {
            AlertDialog.Builder(this)
                .setTitle("You won!")
                .setMessage("Begin again?")
                .setPositiveButton("Yes") {
                        dialog, which -> recreate()
                }
                .show()
        }
    }

    suspend fun flipWithDelay(v: View) {
        val card = v.tag as MemoryGameCard

        if (flippedNow < 2) {
            card.flip()
            flippedNow++

            if (flippedNow == 2) {
                compareFlipped()
                winConditionCheck()
            }
        }
    }

    suspend fun openCards() {

    }

    // обработчик нажатия на кнопку
    val colorListener = View.OnClickListener() {
        // запуск функции в фоновом потоке
        GlobalScope.launch (Dispatchers.Main)
        { flipWithDelay(it) }
    }
}